#!/bin/sh

set -e

# substitute with env variables and create default conf.
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# run nginx
nginx -g 'daemon off;'